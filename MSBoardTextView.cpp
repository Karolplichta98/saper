//
// Created by karol on 2019-04-07.
//

#include "MSBoardTextView.h"
#include "MineSweeper.h"
#include <string>
#include <iostream>

void MSBoardTextView::debug() const {
    for (int i = 0; i < game.getBoardWidth() ; ++i) {
        for (int j = 0; j < game.getBoardHeight() ; ++j) {
            std::cout<<"[";
            game.getFieldInfo(i,j);
            std::cout<<"]";
        }
        std::cout<<std::endl;
    }
}
MSBoardTextView::MSBoardTextView(MinesweeperBoard &game) : game (game) {};




