//
// Created by karol on 2019-03-19.
//

#include "Minesweeper.h"
#include <iostream>
#include <cmath>
#include <ctime>

bool MinesweeperBoard::isRightField(int x, int y) const {
    if (x < 0 || y < 0 || x > width || y > height) {
        return false;}
    return true;
}

int MinesweeperBoard::getBoardWidth() const {
    return width;
}
int MinesweeperBoard::getMineCount() const {
    std::cout<<Mines;
    return Mines;

}

MinesweeperBoard::MinesweeperBoard() {
    width=10;
    height=10;
}
void MinesweeperBoard::setField(int width, int height, bool hasMine, bool isRevealed, bool hasFlag) {
    for (int i = 0; i <=width ;i++) {
        for (int j = 0; j <=height ; j++) {
            board[width][height]={hasMine,isRevealed,hasFlag};

        };

    }
}
void MinesweeperBoard::setBoard()  {
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            setField(i,j,false,false,false);

        }

    }

}
void MinesweeperBoard::debug_display() const {
    for (int i = 0; i < width; i++) {
        for (int c = 0; c < height; c++) {
            std::cout << "[";
            if (board[i][c].hasMine) {
                std::cout << "M";
            } else {
                std::cout << ".";
            }
            if (board[i][c].isRevealed) {
                std::cout << "o";
            } else {
                std::cout << ".";
            }
            if (board[i][c].hasFlag) {
                std::cout << "f";
            } else {
                std::cout << ".";
            }
            std::cout << "]";
        }
        std::cout << std::endl;
    }

}
MinesweeperBoard::MinesweeperBoard(int x, int y,GameMode mode) {
    height = y;
    width = x;
    int MinePlaceX;
    int MinePlaceY;
    srand(time(NULL));
    setBoard();
    if (mode == EASY) {
        Mines = 0.1 * width * height;
        for (int i = 0; i <= Mines - 1; ++i) {
            MinePlaceX = rand() % width;
            MinePlaceY = rand() % height;
            while (board[MinePlaceX][MinePlaceY].hasMine == true) {
                MinePlaceX = rand() % width;
                MinePlaceY = rand() % height;

            }
            board[MinePlaceX][MinePlaceY].hasMine = true;
        }


    }
    if (mode == NORMAL) {
        Mines = 0.2 * width * height;
        for (int i = 0; i <= Mines - 1; ++i) {
            MinePlaceX = rand() % width;
            MinePlaceY = rand() % height;
            while (board[MinePlaceX][MinePlaceY].hasMine == true) {
                MinePlaceX = rand() % width;
                MinePlaceY = rand() % height;

            }
            board[MinePlaceX][MinePlaceY].hasMine = true;
        }

    }
    if (mode == HARD) {
        Mines = 0.3 * width * height;
        for (int i = 0; i <= Mines - 1; ++i) {
            MinePlaceX = rand() % width;
            MinePlaceY = rand() % height;
            while (board[MinePlaceX][MinePlaceY].hasMine == true) {
                MinePlaceX = rand() % width;
                MinePlaceY = rand() % height;

            }
            board[MinePlaceX][MinePlaceY].hasMine = true;
        }
    }

    if (mode == TEST) {
        bool boardmines[10][10] = {
                {1, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 1, 0, 0, 1, 0},
                {0, 0, 0, 1, 1, 0, 0, 0, 0, 0},
                {0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 1, 0, 1, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 1}};
        width = 10;
        height = 10;
        for (int x = 0; x < width; x++){
            for (int y = 0; y < height; y++){
                board[x][y].hasMine = boardmines[x][y];

                if (board[x][y].hasMine) {
            Mines++;}}}

    }
}
bool MinesweeperBoard::hasMine(int x, int y) const {
    if(board[x][y].hasMine) {
        return true;
    }
}
bool MinesweeperBoard::isRevealed(int x, int y) const {
    if (board[x][y].isRevealed) {
        return true;
    }
}

    int MinesweeperBoard::getBoardHeight() const {
        return height;
    }
    int MinesweeperBoard::countMines(int x, int y) const {
        int Miny = 0;
        for (int i = (x - 1); i <= x + 1; ++i) {
            for (int j = y - 1; j <= y + 1; ++j) {
                if (board[i][j].hasMine) {
                    Miny++;
                }
            }

        }
        return Miny;
    }
    bool MinesweeperBoard::hasFlag(int x, int y) const {
        if (isRightField(x,y)==false)
            return false;
        if (board[x][y].isRevealed==true)
            return false;
        if( board[x][y].hasFlag==true) {
            return false;
        }
    }
    void MinesweeperBoard::toggleFlag(int x, int y) {
        if (board[x][y].isRevealed== false) {
            board[x][y].hasFlag=true;
        }
        if (isRightField(x,y)==false) {
            return;
        }
        if (board[x][y].isRevealed==true ) {
            return;
        }
        if (GameState::FINISHED_WIN==true ||GameState::FINISHED_LOSS==true) {
            return;
        }


}
    void MinesweeperBoard::revealField(int x, int y) {

    int MinePlaceX;
    int MinePlaceY;
        if (!board[x][y].isRevealed) {

            board[x][y].isRevealed=true;
        }
        if (board[x][y].isRevealed) {
            Revealed++;
        }
        if (board[x][y].hasMine && Revealed<=1) {
            board[x][y].hasMine=false;
            for (int i = 0; i < width; ++i) {
                for (int j = 0; j < height; ++j) {
                    MinePlaceX = rand() % width;
                    MinePlaceY = rand() % width;
                    while (board[MinePlaceX][MinePlaceY].hasMine == true) {
                        MinePlaceX = rand() % width;
                        MinePlaceY = rand() % height;
                    }
                }
            }
            board[MinePlaceX][MinePlaceY].hasMine = true;
        }

        if (board[x][y].hasFlag) {
            return;
        }
        if (board[x][y].isRevealed){
            return;
        }
        if (!isRightField(x, y)) {
            return;
        }
        if (GameState::FINISHED_WIN || GameState::FINISHED_LOSS){
            return;
        }



    }
    GameState MinesweeperBoard::getGameState() const {



        for (int x = 0; x < width; ++x) {
            for (int m = 0; m < height; ++m) {
                if (board[x][m].hasMine && board[x][m].isRevealed) {
                    GameState::FINISHED_LOSS;
                    std::cout << "Defeat";
                }


                if (Revealed == width * height - Mines) {
                    GameState::FINISHED_WIN;
                    std::cout << "Win";
                }
                if (board[x][m].hasMine && board[x][m].hasFlag) {
                    GameState::FINISHED_WIN;
                    std::cout << "Win";
                }
                if (!GameState::FINISHED_WIN || !GameState::FINISHED_LOSS) {
                    GameState::RUNNING;

                }

            }
        }

    }
char MinesweeperBoard::getFieldInfo(int x, int y) const {
    if (isRightField(x,y)==false) {
        std::cout<<"#";
    }
    if (!board[x][y].isRevealed && board[x][y].hasFlag) {
        std::cout<<"F";
    }
    if (!board[x][y].isRevealed && !board[x][y].hasFlag) {
        std::cout<<"_";
}
    if (board[x][y].isRevealed && board[x][y].hasMine) {
        std::cout << "X";
    }
    if (board[x][y].isRevealed && countMines(x,y)==0) {
        std::cout << "0";
        }
        if (board[x][y].isRevealed && countMines(x, y) != 0) {
            std::cout << countMines(x, y);
        }
    }