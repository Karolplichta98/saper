//
// Created by karol on 2019-04-09.
//

#ifndef MINESWEEPER_MSTEXTCONTROLLER_H
#define MINESWEEPER_MSTEXTCONTROLLER_H
#include "Minesweeper.h"
#include "MSBoardTextView.h"
#include "MSBoardTextView.h"

class MSTextController {MinesweeperBoard &board; const MSBoardTextView &view;
public:
    MSTextController(MinesweeperBoard &board, MSBoardTextView &view);
public:

    void play();
    void display();
    void move ();

};


#endif //MINESWEEPER_MSTEXTCONTROLLER_H
