//
// Created by karol on 2019-03-19.
//

#ifndef MINESWEEPER_MINESWEEPER_H
#define MINESWEEPER_MINESWEEPER_H

enum GameMode  { DEBUG, EASY, NORMAL, HARD ,TEST };
enum GameState { RUNNING, FINISHED_WIN, FINISHED_LOSS };

struct Field
{
    bool hasMine;
    bool hasFlag;
    bool isRevealed;
};

class MinesweeperBoard
{
    Field board[100][100];
    int width;
    int height;
    int Mines=0;
    int Revealed=0;

public:

    MinesweeperBoard();
    MinesweeperBoard(int boardWidth,int boardHeight,GameMode mode);
    void debug_display() const;
    void setBoard() ;
    void setField(int width,int height,bool hasMine,bool hasFlag,bool isRevealed);
    int countMines(int x,int y) const;
    int getBoardWidth() const;
    int getBoardHeight() const;
    int getMineCount() const;
    bool isRightField(int x,int y) const;
    bool hasFlag(int x, int y) const;
    void toggleFlag(int x,int y);
    GameState getGameState() const ;
    void revealField(int x,int y);
    bool hasMine(int x,int y) const;
    bool isRevealed(int x,int y)const;
    char getFieldInfo(int x, int y) const;
};



#endif //MINESWEEPER_MINESWEEPER_H
