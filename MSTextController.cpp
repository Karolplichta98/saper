//
// Created by karol on 2019-04-09.
//

#include "MSTextController.h"
#include "MSBoardTextView.h"
#include "MineSweeper.h"
#include <cctype>
#include <iostream>



    MSTextController::MSTextController(MinesweeperBoard &board, MSBoardTextView &view) : board(board), view(view)
    {}

    void MSTextController::play(){
        while(board.getGameState() == RUNNING)
        {
         view.debug();
move();
        }
    }



    void MSTextController::move()
    {
        char a;
        int x, y;
        std::cin >> a >> x >>y ;
        if(tolower(a) == 'f')
            board.toggleFlag(x,y);
        else if(tolower(a) == 'r')
            board.revealField(x,y);

    };